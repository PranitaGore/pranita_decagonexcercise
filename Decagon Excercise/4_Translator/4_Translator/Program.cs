﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Translator
{
    class Program
    {
        static void Main(string[] args)
        {
            //Dictionary<string, string> dict = new Dictionary<string, string>();
            //dict.Add("Aai", "Aai");
            //dict.Add("Maa", "Maa");
            //dict.Add("Mammy", "Mammy");
            //var myKey = dict.FirstOrDefault(x => x.Value == "Aai").Key;
            //Console.WriteLine(myKey);
            Console.WriteLine("Enter word for search: ");
            string inputFromUser = Console.ReadLine();
            var items = new List<KeyValuePair<string, String>>();
            items.Add(new KeyValuePair<string, String>("Mom", "aai"));
            items.Add(new KeyValuePair<string, String>("Mom", "maa"));
            items.Add(new KeyValuePair<string, String>("Mom", "matoshri"));
            items.Add(new KeyValuePair<string, String>("Bread", "poli"));
            items.Add(new KeyValuePair<string, String>("Bread", "roti"));
            items.Add(new KeyValuePair<string, String>("Bread", "chapati"));
            items.Add(new KeyValuePair<string, String>("train", "aag gadi"));
            items.Add(new KeyValuePair<string, String>("train", "rail gadi"));
            items.Add(new KeyValuePair<string, String>("water", "paani"));
            var lookup = items.ToLookup(kvp => kvp.Value,kvp => kvp.Key);
            foreach (string x in lookup[inputFromUser.ToLower()])
            {
                Console.WriteLine(x);
            }
            Console.ReadLine();
        }
    }
}
