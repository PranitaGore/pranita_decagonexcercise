﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_SellingItems
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> lst = new List<string>{ "iOne", "iTwo", "iThree", "iFour", "iFive" };
            Console.WriteLine("Select items you want to buy from list:iOne,iTwo,iThree,iFour,iFive");
            string[] arr=Console.ReadLine().Split(',');
            List<int> cntList = new List<int>();
            Dictionary<string, int> dict = new Dictionary<string, int>();
            int count = 0;
            foreach(string str in lst)
            {
                if(lst.Contains(str))
                {
                    count = arr.Where(c => c == str).Count();
                    cntList.Add(count);
                    dict.Add(str, count);
                }
                else
                {
                    Console.WriteLine("This item is not present in list: "+str);
                }
                 
            }
            //Console.WriteLine("count: "+cntList.Max());
            Console.WriteLine("count: "+ dict.FirstOrDefault(x => x.Value == cntList.Max()).Key);
            Console.ReadLine();
        }
    }
}
