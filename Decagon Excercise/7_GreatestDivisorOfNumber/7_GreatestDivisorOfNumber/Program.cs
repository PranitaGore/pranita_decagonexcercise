﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7_GreatestDivisorOfNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter number: ");
            int number = int.Parse(Console.ReadLine());
            if (number < 1000 || number > 99)
            {
                int result = CalGreatestDivisor(number);
                Console.WriteLine("Greatest divisor of your number is:" + result);
            }
            else
            {
                Console.WriteLine("Please Print 3 Digit Number....");

            }
            Console.ReadLine();
        }
        public static int CalGreatestDivisor(int number)
        {
            List<int> factors = new List<int>();
            int maxNumber = (int)Math.Sqrt(number); 
            for (int factor = 1; factor <= maxNumber; ++factor)
            {

                if (number % factor == 0)
                {
                    factors.Add(factor);
                    if (factor != number / factor)
                    {

                        factors.Add(number / factor);
                    }
                }
            }

            // return factors.Max(); //divisor
            int[] intArr = factors.ToArray();
            int max2 = intArr.OrderByDescending(T => T).ElementAt(1);
            return max2;  //greatest divisor(except 1)
        }
    }
}
